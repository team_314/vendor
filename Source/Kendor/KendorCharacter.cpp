// Copyright Epic Games, Inc. All Rights Reserved.

#include "KendorCharacter.h"

#include "Kismet/KismetMathLibrary.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

AKendorCharacter::AKendorCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->SocketOffset = FVector(0.f,0.f,75.f);
	CameraBoom->SetRelativeRotation(FRotator(0.f,180.f,0.f));
	CameraBoom->bEnableCameraLag = bLag;
	CameraBoom->CameraLagSpeed = CameraBoom->bEnableCameraLag ? LagSpeed : NULL;
	CameraBoom->TargetArmLength = Distance;
	CameraBoom->SetRelativeRotation({AngleOfCamera, 180,  0});

	// Create a camera and attach to boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = 2.f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void AKendorCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("ShakeCamera", IE_Pressed, this, &AKendorCharacter::PlayCameraShake);
	PlayerInputComponent->BindAxis("MoveRight", this, &AKendorCharacter::MoveRight);

}

void AKendorCharacter::MoveRight(float Value)
{
	static float PrevValue = NULL;
	
	if(Value)
	{
		SetActorRotation({0.f, -90.f * Value, 0.f});
		AddMovementInput(FVector(0.f,-1.f,0.f), Value);
		CameraBoom->SetRelativeLocation({CameraShift, 0.f, 0.f});

		PrevValue = 1;
	}
	else if(PrevValue)
	{
		CameraBoom->SetRelativeLocation({0.f, 0.f, 0.f});
		PrevValue = 0;
	}
	
}

void AKendorCharacter::PlayCameraShake()
{
	GetWorld()->GetFirstPlayerController()->ClientStartCameraShake(CameraShake);
}