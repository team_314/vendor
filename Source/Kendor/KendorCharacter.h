// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Effects/CamShake.h"
#include "KendorCharacter.generated.h"

UCLASS(config=Game)
class AKendorCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = Camera)
	bool bLag = true;
	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float LagSpeed = 3.0; 
	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float Distance = 500.0;
	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float AngleOfCamera = -10;
	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float CameraShift = 400;
	UPROPERTY(EditDefaultsOnly, Category = Camera)
	TSubclassOf<UCamShake> CameraShake;
	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UFUNCTION()
	void PlayCameraShake();

protected:

	/** Called for side to side input */
	void MoveRight(float Val);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface


public:
	AKendorCharacter();

	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
};
