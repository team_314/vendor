// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "KendorGameMode.generated.h"

UCLASS(minimalapi)
class AKendorGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AKendorGameMode();
};



