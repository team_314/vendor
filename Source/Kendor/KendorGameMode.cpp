// Copyright Epic Games, Inc. All Rights Reserved.

#include "KendorGameMode.h"
#include "KendorCharacter.h"
#include "UObject/ConstructorHelpers.h"

AKendorGameMode::AKendorGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/SideScrollerCPP/Blueprints/SideScrollerCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
