// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Kendor : ModuleRules
{
	public Kendor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });
	}
}
