// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MatineeCameraShake.h"
#include "CamShake.generated.h"

/**
 * 
 */
UCLASS()
class KENDOR_API UCamShake : public UMatineeCameraShake
{
	GENERATED_BODY()

	UCamShake();
	
};
