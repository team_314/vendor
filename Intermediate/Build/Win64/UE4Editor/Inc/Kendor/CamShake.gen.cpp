// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Kendor/Effects/CamShake.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCamShake() {}
// Cross Module References
	KENDOR_API UClass* Z_Construct_UClass_UCamShake_NoRegister();
	KENDOR_API UClass* Z_Construct_UClass_UCamShake();
	GAMEPLAYCAMERAS_API UClass* Z_Construct_UClass_UMatineeCameraShake();
	UPackage* Z_Construct_UPackage__Script_Kendor();
// End Cross Module References
	void UCamShake::StaticRegisterNativesUCamShake()
	{
	}
	UClass* Z_Construct_UClass_UCamShake_NoRegister()
	{
		return UCamShake::StaticClass();
	}
	struct Z_Construct_UClass_UCamShake_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCamShake_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMatineeCameraShake,
		(UObject* (*)())Z_Construct_UPackage__Script_Kendor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCamShake_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "CameraShakePattern" },
		{ "IncludePath", "Effects/CamShake.h" },
		{ "ModuleRelativePath", "Effects/CamShake.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCamShake_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCamShake>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCamShake_Statics::ClassParams = {
		&UCamShake::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCamShake_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCamShake_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCamShake()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCamShake_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCamShake, 3486788162);
	template<> KENDOR_API UClass* StaticClass<UCamShake>()
	{
		return UCamShake::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCamShake(Z_Construct_UClass_UCamShake, &UCamShake::StaticClass, TEXT("/Script/Kendor"), TEXT("UCamShake"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCamShake);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
