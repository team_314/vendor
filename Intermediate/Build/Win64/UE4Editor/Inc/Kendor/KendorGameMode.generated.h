// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef KENDOR_KendorGameMode_generated_h
#error "KendorGameMode.generated.h already included, missing '#pragma once' in KendorGameMode.h"
#endif
#define KENDOR_KendorGameMode_generated_h

#define Kendor_Source_Kendor_KendorGameMode_h_12_SPARSE_DATA
#define Kendor_Source_Kendor_KendorGameMode_h_12_RPC_WRAPPERS
#define Kendor_Source_Kendor_KendorGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Kendor_Source_Kendor_KendorGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAKendorGameMode(); \
	friend struct Z_Construct_UClass_AKendorGameMode_Statics; \
public: \
	DECLARE_CLASS(AKendorGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Kendor"), KENDOR_API) \
	DECLARE_SERIALIZER(AKendorGameMode)


#define Kendor_Source_Kendor_KendorGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAKendorGameMode(); \
	friend struct Z_Construct_UClass_AKendorGameMode_Statics; \
public: \
	DECLARE_CLASS(AKendorGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Kendor"), KENDOR_API) \
	DECLARE_SERIALIZER(AKendorGameMode)


#define Kendor_Source_Kendor_KendorGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	KENDOR_API AKendorGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AKendorGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(KENDOR_API, AKendorGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AKendorGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	KENDOR_API AKendorGameMode(AKendorGameMode&&); \
	KENDOR_API AKendorGameMode(const AKendorGameMode&); \
public:


#define Kendor_Source_Kendor_KendorGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	KENDOR_API AKendorGameMode(AKendorGameMode&&); \
	KENDOR_API AKendorGameMode(const AKendorGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(KENDOR_API, AKendorGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AKendorGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AKendorGameMode)


#define Kendor_Source_Kendor_KendorGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Kendor_Source_Kendor_KendorGameMode_h_9_PROLOG
#define Kendor_Source_Kendor_KendorGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Kendor_Source_Kendor_KendorGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Kendor_Source_Kendor_KendorGameMode_h_12_SPARSE_DATA \
	Kendor_Source_Kendor_KendorGameMode_h_12_RPC_WRAPPERS \
	Kendor_Source_Kendor_KendorGameMode_h_12_INCLASS \
	Kendor_Source_Kendor_KendorGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Kendor_Source_Kendor_KendorGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Kendor_Source_Kendor_KendorGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Kendor_Source_Kendor_KendorGameMode_h_12_SPARSE_DATA \
	Kendor_Source_Kendor_KendorGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Kendor_Source_Kendor_KendorGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Kendor_Source_Kendor_KendorGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> KENDOR_API UClass* StaticClass<class AKendorGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Kendor_Source_Kendor_KendorGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
