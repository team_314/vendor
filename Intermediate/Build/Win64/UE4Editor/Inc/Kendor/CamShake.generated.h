// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef KENDOR_CamShake_generated_h
#error "CamShake.generated.h already included, missing '#pragma once' in CamShake.h"
#endif
#define KENDOR_CamShake_generated_h

#define Kendor_Source_Kendor_Effects_CamShake_h_15_SPARSE_DATA
#define Kendor_Source_Kendor_Effects_CamShake_h_15_RPC_WRAPPERS
#define Kendor_Source_Kendor_Effects_CamShake_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Kendor_Source_Kendor_Effects_CamShake_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCamShake(); \
	friend struct Z_Construct_UClass_UCamShake_Statics; \
public: \
	DECLARE_CLASS(UCamShake, UMatineeCameraShake, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Kendor"), NO_API) \
	DECLARE_SERIALIZER(UCamShake)


#define Kendor_Source_Kendor_Effects_CamShake_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUCamShake(); \
	friend struct Z_Construct_UClass_UCamShake_Statics; \
public: \
	DECLARE_CLASS(UCamShake, UMatineeCameraShake, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Kendor"), NO_API) \
	DECLARE_SERIALIZER(UCamShake)


#define Kendor_Source_Kendor_Effects_CamShake_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCamShake(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCamShake) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCamShake); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCamShake); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCamShake(UCamShake&&); \
	NO_API UCamShake(const UCamShake&); \
public:


#define Kendor_Source_Kendor_Effects_CamShake_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCamShake(UCamShake&&); \
	NO_API UCamShake(const UCamShake&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCamShake); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCamShake); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCamShake)


#define Kendor_Source_Kendor_Effects_CamShake_h_15_PRIVATE_PROPERTY_OFFSET
#define Kendor_Source_Kendor_Effects_CamShake_h_12_PROLOG
#define Kendor_Source_Kendor_Effects_CamShake_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Kendor_Source_Kendor_Effects_CamShake_h_15_PRIVATE_PROPERTY_OFFSET \
	Kendor_Source_Kendor_Effects_CamShake_h_15_SPARSE_DATA \
	Kendor_Source_Kendor_Effects_CamShake_h_15_RPC_WRAPPERS \
	Kendor_Source_Kendor_Effects_CamShake_h_15_INCLASS \
	Kendor_Source_Kendor_Effects_CamShake_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Kendor_Source_Kendor_Effects_CamShake_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Kendor_Source_Kendor_Effects_CamShake_h_15_PRIVATE_PROPERTY_OFFSET \
	Kendor_Source_Kendor_Effects_CamShake_h_15_SPARSE_DATA \
	Kendor_Source_Kendor_Effects_CamShake_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Kendor_Source_Kendor_Effects_CamShake_h_15_INCLASS_NO_PURE_DECLS \
	Kendor_Source_Kendor_Effects_CamShake_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> KENDOR_API UClass* StaticClass<class UCamShake>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Kendor_Source_Kendor_Effects_CamShake_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
