// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Kendor/KendorCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeKendorCharacter() {}
// Cross Module References
	KENDOR_API UClass* Z_Construct_UClass_AKendorCharacter_NoRegister();
	KENDOR_API UClass* Z_Construct_UClass_AKendorCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_Kendor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	KENDOR_API UClass* Z_Construct_UClass_UCamShake_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AKendorCharacter::execPlayCameraShake)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->PlayCameraShake();
		P_NATIVE_END;
	}
	void AKendorCharacter::StaticRegisterNativesAKendorCharacter()
	{
		UClass* Class = AKendorCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "PlayCameraShake", &AKendorCharacter::execPlayCameraShake },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AKendorCharacter_PlayCameraShake_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AKendorCharacter_PlayCameraShake_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "KendorCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AKendorCharacter_PlayCameraShake_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AKendorCharacter, nullptr, "PlayCameraShake", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00040401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AKendorCharacter_PlayCameraShake_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AKendorCharacter_PlayCameraShake_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AKendorCharacter_PlayCameraShake()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AKendorCharacter_PlayCameraShake_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AKendorCharacter_NoRegister()
	{
		return AKendorCharacter::StaticClass();
	}
	struct Z_Construct_UClass_AKendorCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLag_MetaData[];
#endif
		static void NewProp_bLag_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLag;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LagSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LagSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Distance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Distance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngleOfCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngleOfCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraShift_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CameraShift;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraShake_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_CameraShake;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SideViewCameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SideViewCameraComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AKendorCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_Kendor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AKendorCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AKendorCharacter_PlayCameraShake, "PlayCameraShake" }, // 518890205
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AKendorCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "KendorCharacter.h" },
		{ "ModuleRelativePath", "KendorCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AKendorCharacter_Statics::NewProp_bLag_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "KendorCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AKendorCharacter_Statics::NewProp_bLag_SetBit(void* Obj)
	{
		((AKendorCharacter*)Obj)->bLag = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AKendorCharacter_Statics::NewProp_bLag = { "bLag", nullptr, (EPropertyFlags)0x0040000000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AKendorCharacter), &Z_Construct_UClass_AKendorCharacter_Statics::NewProp_bLag_SetBit, METADATA_PARAMS(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_bLag_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_bLag_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AKendorCharacter_Statics::NewProp_LagSpeed_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "KendorCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AKendorCharacter_Statics::NewProp_LagSpeed = { "LagSpeed", nullptr, (EPropertyFlags)0x0040000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AKendorCharacter, LagSpeed), METADATA_PARAMS(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_LagSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_LagSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AKendorCharacter_Statics::NewProp_Distance_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "KendorCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AKendorCharacter_Statics::NewProp_Distance = { "Distance", nullptr, (EPropertyFlags)0x0040000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AKendorCharacter, Distance), METADATA_PARAMS(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_Distance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_Distance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AKendorCharacter_Statics::NewProp_AngleOfCamera_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "KendorCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AKendorCharacter_Statics::NewProp_AngleOfCamera = { "AngleOfCamera", nullptr, (EPropertyFlags)0x0040000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AKendorCharacter, AngleOfCamera), METADATA_PARAMS(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_AngleOfCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_AngleOfCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraShift_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "KendorCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraShift = { "CameraShift", nullptr, (EPropertyFlags)0x0040000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AKendorCharacter, CameraShift), METADATA_PARAMS(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraShift_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraShift_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraShake_MetaData[] = {
		{ "Category", "Camera" },
		{ "ModuleRelativePath", "KendorCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraShake = { "CameraShake", nullptr, (EPropertyFlags)0x0044000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AKendorCharacter, CameraShake), Z_Construct_UClass_UCamShake_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraShake_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraShake_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AKendorCharacter_Statics::NewProp_SideViewCameraComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Side view camera */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "KendorCharacter.h" },
		{ "ToolTip", "Side view camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AKendorCharacter_Statics::NewProp_SideViewCameraComponent = { "SideViewCameraComponent", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AKendorCharacter, SideViewCameraComponent), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_SideViewCameraComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_SideViewCameraComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Camera boom positioning the camera beside the character */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "KendorCharacter.h" },
		{ "ToolTip", "Camera boom positioning the camera beside the character" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraBoom = { "CameraBoom", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AKendorCharacter, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraBoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraBoom_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AKendorCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AKendorCharacter_Statics::NewProp_bLag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AKendorCharacter_Statics::NewProp_LagSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AKendorCharacter_Statics::NewProp_Distance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AKendorCharacter_Statics::NewProp_AngleOfCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraShift,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraShake,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AKendorCharacter_Statics::NewProp_SideViewCameraComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AKendorCharacter_Statics::NewProp_CameraBoom,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AKendorCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AKendorCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AKendorCharacter_Statics::ClassParams = {
		&AKendorCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AKendorCharacter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AKendorCharacter_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AKendorCharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AKendorCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AKendorCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AKendorCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AKendorCharacter, 4068990191);
	template<> KENDOR_API UClass* StaticClass<AKendorCharacter>()
	{
		return AKendorCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AKendorCharacter(Z_Construct_UClass_AKendorCharacter, &AKendorCharacter::StaticClass, TEXT("/Script/Kendor"), TEXT("AKendorCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AKendorCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
