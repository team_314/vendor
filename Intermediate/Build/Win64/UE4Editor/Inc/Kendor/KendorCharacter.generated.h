// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef KENDOR_KendorCharacter_generated_h
#error "KendorCharacter.generated.h already included, missing '#pragma once' in KendorCharacter.h"
#endif
#define KENDOR_KendorCharacter_generated_h

#define Kendor_Source_Kendor_KendorCharacter_h_13_SPARSE_DATA
#define Kendor_Source_Kendor_KendorCharacter_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPlayCameraShake);


#define Kendor_Source_Kendor_KendorCharacter_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPlayCameraShake);


#define Kendor_Source_Kendor_KendorCharacter_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAKendorCharacter(); \
	friend struct Z_Construct_UClass_AKendorCharacter_Statics; \
public: \
	DECLARE_CLASS(AKendorCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Kendor"), NO_API) \
	DECLARE_SERIALIZER(AKendorCharacter)


#define Kendor_Source_Kendor_KendorCharacter_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAKendorCharacter(); \
	friend struct Z_Construct_UClass_AKendorCharacter_Statics; \
public: \
	DECLARE_CLASS(AKendorCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Kendor"), NO_API) \
	DECLARE_SERIALIZER(AKendorCharacter)


#define Kendor_Source_Kendor_KendorCharacter_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AKendorCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AKendorCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AKendorCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AKendorCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AKendorCharacter(AKendorCharacter&&); \
	NO_API AKendorCharacter(const AKendorCharacter&); \
public:


#define Kendor_Source_Kendor_KendorCharacter_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AKendorCharacter(AKendorCharacter&&); \
	NO_API AKendorCharacter(const AKendorCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AKendorCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AKendorCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AKendorCharacter)


#define Kendor_Source_Kendor_KendorCharacter_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bLag() { return STRUCT_OFFSET(AKendorCharacter, bLag); } \
	FORCEINLINE static uint32 __PPO__LagSpeed() { return STRUCT_OFFSET(AKendorCharacter, LagSpeed); } \
	FORCEINLINE static uint32 __PPO__Distance() { return STRUCT_OFFSET(AKendorCharacter, Distance); } \
	FORCEINLINE static uint32 __PPO__AngleOfCamera() { return STRUCT_OFFSET(AKendorCharacter, AngleOfCamera); } \
	FORCEINLINE static uint32 __PPO__CameraShift() { return STRUCT_OFFSET(AKendorCharacter, CameraShift); } \
	FORCEINLINE static uint32 __PPO__CameraShake() { return STRUCT_OFFSET(AKendorCharacter, CameraShake); } \
	FORCEINLINE static uint32 __PPO__SideViewCameraComponent() { return STRUCT_OFFSET(AKendorCharacter, SideViewCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AKendorCharacter, CameraBoom); }


#define Kendor_Source_Kendor_KendorCharacter_h_10_PROLOG
#define Kendor_Source_Kendor_KendorCharacter_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Kendor_Source_Kendor_KendorCharacter_h_13_PRIVATE_PROPERTY_OFFSET \
	Kendor_Source_Kendor_KendorCharacter_h_13_SPARSE_DATA \
	Kendor_Source_Kendor_KendorCharacter_h_13_RPC_WRAPPERS \
	Kendor_Source_Kendor_KendorCharacter_h_13_INCLASS \
	Kendor_Source_Kendor_KendorCharacter_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Kendor_Source_Kendor_KendorCharacter_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Kendor_Source_Kendor_KendorCharacter_h_13_PRIVATE_PROPERTY_OFFSET \
	Kendor_Source_Kendor_KendorCharacter_h_13_SPARSE_DATA \
	Kendor_Source_Kendor_KendorCharacter_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Kendor_Source_Kendor_KendorCharacter_h_13_INCLASS_NO_PURE_DECLS \
	Kendor_Source_Kendor_KendorCharacter_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> KENDOR_API UClass* StaticClass<class AKendorCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Kendor_Source_Kendor_KendorCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
